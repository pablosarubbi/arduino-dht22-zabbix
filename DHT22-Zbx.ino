/******************************************************************************************************
  Implementacion de sistema de monitoreo de temepratura y humedad con sensor DHT22 y 
  agente Zabbix.
  Version: 0.8 - Implementacion de funcion zbx_print() para enviar el mensaje con protocolo Zabbix
  Fecha: 04/07/2019
  - Monitor de temperatura y humedad con implementación de agente Zabbix
  Mas informacion: https://pablo.sarubbi.com.ar
*******************************************************************************************************
/*
El Sketch usa 19194 bytes (59%) del espacio de almacenamiento de programa. El máximo es 32256 bytes.
Las variables Globales usan 666 bytes (32%) de la memoria dinámica, dejando 1382 bytes para las variables locales. El máximo es 2048 bytes.
*/

#define DEBUG_SERVICE

#include <Ethernet.h>                                                               // Libreria para manejo de pila de TCP/IP  
#include <DHT.h>                                                                    // Libreria que implementa de manera sencilla la lectura del sensor de temperatura/humedad

String zbx_cmd ="";

EthernetServer zServer(10050);

static byte mac[6]                      = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEE};
static byte ip[4]                       = {192, 168, 0, 76};

#define PIN_DHT 9                                                                   // Nro. de pin donde se encuentra conectado el sensor
#define DHTTYPE DHT22                                                               // Modelo de sensor, puede ser DHT11, DHT21 o DHT22
DHT dht(PIN_DHT, DHTTYPE);                                                          // Inicializacion del sensor

unsigned long prevMillisGetTemp         = millis();

float humidity                          = 0;
float temperature                       = 0;

void setup() {
  #ifdef DEBUG_SERVICE
    Serial.begin(9600);
  #endif
  
  dht.begin();
  Ethernet.begin(mac, ip);
  zServer.begin();  
}

void loop() {
  if (millis() - prevMillisGetTemp > 3000) {
    humidity = dht.readHumidity();
    temperature = dht.readTemperature();
    prevMillisGetTemp = millis();

    #ifdef DEBUG_SERVICE
      Serial.print(F("Temperatura: "));
      Serial.println(temperature);
      Serial.print(F("Humedad: "));
      Serial.println(humidity);
    #endif
  }

  EthernetClient zClient = zServer.available();
  
  if (zClient) {
    if (zClient.available() > 0) {
      char thisChar = zClient.read();
      if( thisChar == 0x01 ) {                                                      // Busco un byte en 1 como separador del header y el lenght del mensaje 
        thisChar = zClient.read();                                                  // 
        for (int i = 1; i < 8; i++){
           zClient.read();
        }
        
        for ( int i = int (thisChar); i > 0; i--){
          thisChar = zClient.read();
          zbx_cmd += thisChar;
        }
        
        if(zbx_cmd.indexOf("agent.ping") >= 0) {
          byte zbx_msg[] = {0x31};
          zbx_print(zbx_msg, sizeof(zbx_msg));
        } else if(zbx_cmd.equals("agent.hostname")) {
          byte zbx_msg[] = { 'A', 'r', 'd', 'u', 'i', 'n', 'o', '_', 'S', 'e', 'n', 's', 'o', 'r' };
          zbx_print(zbx_msg, sizeof(zbx_msg));
          } else if(zbx_cmd.equals("agent.version")) {
            byte zbx_msg[] = { '0', '.', '8', '0'};
            zbx_print(zbx_msg, sizeof(zbx_msg));
        } else if(zbx_cmd.equals("dht.temperature")) {
          char zbx_msg[8];
          dtostrf(temperature, 6, 2, zbx_msg);
          int len = sizeof(zbx_msg);
          zbx_print(zbx_msg, sizeof(zbx_msg));
        } else if(zbx_cmd.equals("dht.humidity")) {
          char zbx_msg[8];                                                                      
          dtostrf(humidity, 6, 2, zbx_msg);
          zbx_print(zbx_msg, sizeof(zbx_msg));
        } else {  // ZBX_NOTSUPPORTED
          byte zbx_msg[] = {'Z', 'B', 'X', '_', 'N', 'O', 'T', 'S', 'U', 'P', 'P', 'O', 'R', 'T', 'E', 'D', 0x00, 'U', 'n', 's', 'u', 'p', 'p', 'o', 'r', 't', 'e', 'd', ' ', 'i', 't', 'e', 'm', ' ', 'k', 'e', 'y', '.' };
          zbx_print(zbx_msg, sizeof(zbx_msg));
        }
        zClient.stop();
        zbx_cmd ="";
      }
      zbx_cmd ="";
    }
  }
  delay(1);
}

/*
      URL: https://www.zabbix.com/documentation/3.4/manual/appendix/protocols/header_datalen
      4 Header and data length - Overview
      Header and data length are present in response and request messages between Zabbix components. It is required to determine the length of message.
      <HEADER> - "ZBXD\x01" (5 bytes)
      <DATALEN> - data length (8 bytes). 1 will be formatted as 01/00/00/00/00/00/00/00 (eight bytes, 64 bit number in little-endian format)
      To not exhaust memory (potentially) Zabbix protocol is limited to accept only 128MB in one connection.
*/

void zbx_print(byte my_msg[], int len_msg){
  static const byte zbx_head[5]           = { 'Z', 'B', 'X', 'D', 0x01 };             // ZBXD\1
  byte my_len[8] = {len_msg, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  zServer.write(zbx_head,5);
  zServer.write(my_len,8);
  zServer.write(my_msg,len_msg);
}

